var express = require('express');            // módulo express
var app = express();
var server = require('http').Server(app);	 // objeto express
var io = require('socket.io')(server);
var bodyParser = require('body-parser');     // processa corpo de requests
var cookieParser = require('cookie-parser'); // processa cookies
var path = require('path');                  // caminho de arquivos
var amqp = require('amqplib/callback_api');  // comunicação amqp



server.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});


app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
app.use(express.static('public'));

var id_gen = 0;    // Gerador de ID
var users = {};    // Usuários
var channels = {}; // Lista de canais
var amqp_conn;
var amqp_ch;

// Estabelece conexão com o servidor AMQP antes de qualquer cliente se conectar
amqp.connect('amqp://localhost', function(err, conn) {
	conn.createChannel(function(err, ch) {
		amqp_conn = conn;
		amqp_ch = ch;
	});
});


io.on('connection', function (socket) {
//  console.log('new conection from '+ socket.id);
});

// Realiza login gravando dados nos cookies
app.post('/login', function (req, res) { 
	
	res.cookie('nick', req.body.nome);
	if(req.body.canal && req.body.canal[0]!='#'){
		req.body.canal = '#'+req.body.canal;
	}
	res.cookie('canal', req.body.canal);
	res.cookie('servidor', req.body.servidor);
	res.redirect('/');
});

function enviarParaServidor (nomeFila, msg) {
	msg = new Buffer(JSON.stringify(msg));
	amqp_ch.assertQueue(nomeFila, {durable: false});
	amqp_ch.sendToQueue(nomeFila, msg);
//	console.log(" [app] Sent %s", msg);
}

function receberDoServidor (id, callback) {
	amqp_ch.assertQueue("user_"+id, {durable: false});
//	console.log(" [app] Waiting for messages for "+ id);
	amqp_ch.consume("user_"+id, function(msg) {
//		console.log(" [app] ID "+id+" Received "+msg.content.toString());
		callback(id, JSON.parse(msg.content.toString()));
        //callback(id, msg);
	}, {noAck: true});
}

// Faz o registro de conexão com o servidor IRC
app.get('/', function (req, res) {
	
	if ( req.cookies.servidor && req.cookies.nick  && req.cookies.canal ) {
		
		id_gen++; // Cria um ID para o usuário
		id = id_gen;
		
		// Cria um cache de mensagens
		users[id] = {cache: [{
			    "timestamp": Date.now(),
	   		    "nick": "IRC Server",
                "tipo":"teste",
	   		    "msg": "Bem vindo ao servidor IRC"}]
		};
	   
	   res.cookie('id', id); // Seta o ID nos cookies do cliente
	   
	   var target = 'registro_conexao';
	   var msg = {
		   id: id, 
		   servidor: req.cookies.servidor,
		   nick: req.cookies.nick,
		   canal: req.cookies.canal
	   };
	   
	   users[id].id       = msg.id;
	   users[id].servidor = msg.servidor;
	   users[id].nick     = msg.nick;
	   users[id].canal    = msg.canal;

        // Envia registro de conexão para o servidor
        enviarParaServidor(target, msg);

	   // Se inscreve para receber mensagens endereçadas a este usuário
	   receberDoServidor(id, function (id_real, msg) {
		   //Adiciona mensagem ao cache do usuário
//		   console.log("Mensagem colocada no cache do usuário "+users[id_real].nick);

//             console.log(msg);
		   users[id_real].cache.push(msg);
	   console.log("Mensagem colocada no cache do usuário "+users[id_real].nick);
		   console.log("MENSAGEM msg: "+JSON.stringify(msg.msg));
		   console.log("TIPO: "+typeof JSON.stringify(msg.msg));

		   if (msg.tipo.toString() === 'error'){
               msg.msg = JSON.stringify(msg.msg);
               users[id_real].cache.push(msg);
           }else if(msg.tipo.toString() === 'registered'){
               msg.msg = JSON.stringify(msg.msg);
               users[id_real].cache.push(msg);
           }else if (msg.tipo.toString() === 'motd'){
               msg.msg = JSON.stringify(msg.msg);
               users[id_real].cache.push(msg);
           }else if (msg.tipo.toString() === 'names'){
             if(typeof(msg.nicks) === 'object')
             {
               msg.msg = "Usuários conectados no canal " +msg.channel+": "+ JSON.stringify(msg.nicks);
             }
             else if(msg.nicks.constructor === Array)
             {
                msg.msg = "Usuários conectados no canal " +msg.channel+": "+ msg.nicks.join(' - ');
             }
             else
             {
               msg.msg = "Usuários conectados no canal " +msg.channel+": "+ msg.nicks;
             }

             users[id_real].cache.push(msg);
           }else if (msg.tipo.toString() === 'quit'){
               msg.msg = JSON.stringify(msg.channels)+" : "+JSON.stringify(msg.nick)+" because "+JSON.stringify(msg.msg);
               users[id_real].cache.push(msg);
           }else if (msg.tipo.toString() === 'kick'){
               msg.msg = JSON.stringify(msg.channel)+" : "+JSON.stringify(msg.nick)+" because "+JSON.stringify(msg.msg);
               users[id_real].cache.push(msg);
           }else if (msg.tipo.toString() === 'kill'){
               msg.msg = JSON.stringify(msg.channels)+" : "+JSON.stringify(msg.nick)+" because "+JSON.stringify(msg.msg);
               users[id_real].cache.push(msg);
           }else if (msg.tipo.toString() === 'notice'){
               msg.msg = JSON.stringify(msg.msg);
               users[id_real].cache.push(msg);
           }else if (msg.tipo.toString() === 'channellist'){
               msg.msg = JSON.stringify(msg.msg);
//               console.log(msg);
               users[id_real].cache.push(msg);
           }else if (msg.tipo.toString() === 'channellist_item'){
               msg.msg = JSON.stringify(msg.msg);
               users[id_real].cache.push(msg);
           }else if (msg.tipo.toString() === 'part#teste'){
               msg.msg = JSON.stringify(msg.nick)+" : "+JSON.stringify(msg.msg)+" reason "+JSON.stringify(msg.reason);
               users[id_real].cache.push(msg);
           }else if (msg.tipo.toString() === 'whois'){
               msg.msg = JSON.stringify(msg.msg);
               users[id_real].cache.push(msg);
           }else if (msg.tipo.toString() === 'message') users[id_real].cache.push(msg);
	   });



	   res.sendFile(path.join(__dirname, '/index.html'));
	}
	else {
		res.sendFile(path.join(__dirname, '/login.html'));
	}
});


// Obtém mensagens armazenadas em cache (via polling)
app.get('/obter_mensagem/:timestamp', function (req, res) {

	var id = req.cookies.id;
//	console.log("id: "+id);
//	console.log("user: "+users[id]);


	var response = users[id].cache;
	users.cache = [];

	res.append('Content-type', 'application/json');
//  console.log(response);
	res.send(response);
});

// Envia uma mensagem para o servidor IRC
app.post('/gravar_mensagem', function (req, res) {

	// Adiciona mensagem enviada ao cache do usuário
	users[req.cookies.id].cache.push(req.body);
	enviarParaServidor("gravar_mensagem", {
		id: req.cookies.id,
		nick: users[req.cookies.id].nick,
		canal: users[req.cookies.id].canal,
		msg: req.body.msg.toString()
	});
	res.end();
});
