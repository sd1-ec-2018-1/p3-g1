var irc = require('irc');
var amqp = require('amqplib/callback_api');

var proxies = {}; // mapa de proxys
var amqp_conn;
var amqp_ch;
var irc_clients = {};

// Conexão com o servidor AMQP
amqp.connect('amqp://localhost', function(err, conn) {
	
	conn.createChannel(function(err, ch) {
		amqp_conn = conn;
		amqp_ch = ch;
		inicializar();
	});
});

// Inicializa a comunicação entra Rabbit e Servidor IRC
function inicializar() {
	
	receberDoCliente("registro_conexao", function (msg) {
//		console.log('irc-proxy.js: recebeu registro de conexão');
		var id       = msg.id;
		var servidor = msg.servidor;
		var nick     = msg.nick;
		var canal    = msg.canal;
		
		irc_clients[id] = new irc.Client(
			servidor, 
			nick,
			{channels: [canal]}
		);

		// Verify new messages
		irc_clients[id].addListener('message'+canal, function (from, message) {
//			console.log(from + ' => '+ canal +': ' + message);
			enviarParaCliente(id, {
				"timestamp": Date.now(),
                "tipo":'message',
				"nick": from,
				"msg": message
			});
		});

		// Verify Error
	    irc_clients[id].addListener('error', function(message) {
//		    console.log('error: ', message);
		    enviarParaCliente(id, {
		        "timestamp": Date.now(),
                "nick": "IRC Server",
				"tipo":"error",
		        "msg": message.args[message.args.length - 1]
		    });
		});

          irc_clients[id].addListener('raw', function(message) {
            if(message.command === "rpl_endofnames")
            {
//              console.log("aqui 2 " + JSON.stringify(message));
              var nickArray = message.args[message.args.length - 3];
              var channel = message.args[message.args.length - 2];
  //            console.log("nick array " + nickArray);
    //          console.log("channel array " + channel);
              enviarParaCliente(id, {
                "timestamp": Date.now(),
                "nick": "IRC Server",
				"tipo":"names",
                "channel": channel,
				"nicks": nickArray
              });
            }
          });

		// Indicating you’ve connected to the server
        irc_clients[id].addListener('registered', function(message) {
            //console.log(message);
          message=message.args[1];
            enviarParaCliente(id, {
                "timestamp": Date.now(),
                "nick": "IRC Server",
				"tipo":"registered",
                "msg": message
            });
        });

        // Show message of the day
        irc_clients[id].addListener('motd', function(motd) {
           enviarParaCliente(id, {
                "timestamp": Date.now(),
                "nick": "IRC Server",
				"tipo":"motd",
                "msg": motd
            });
        });

        // Emitted when the server sends a list of nicks for a channel
        irc_clients[id].addListener('names', function(channel,nicks) {
//console.log('aqui '+JSON.stringify(nicks));
            enviarParaCliente(id, {
                "timestamp": Date.now(),
                "nick": "IRC Server",
				"tipo":"names",
                "channel": channel,
				"nicks": nicks
            });
        });

        // Emitted when a user disconnects from the IRC
        irc_clients[id].addListener('quit', function(nick, reason, channels, message) {
            enviarParaCliente(id, {
                "timestamp": Date.now(),
                "nick": "IRC Server",
				"tipo":"quit",
                "channels": channels,
				"msg":"Message: "+message+" reason "+reason+" by "+nick
            });
        });

        // Emitted when a user is kicked from a channel
        irc_clients[id].addListener('kick', function(channel, nick, by, reason, message) {
            enviarParaCliente(id, {
                "timestamp": Date.now(),
				"tipo":"kick",
                "channel": channel,
                "nick": "IRC Server",
                "msg":"Message: "+message+" by "+by+" reason "+reason+" nick "+nick
            });
        });

        // Emitted when a user is killed from the IRC server
        irc_clients[id].addListener('kill', function(nick, reason, channels, message) {
            enviarParaCliente(id, {
                "timestamp": Date.now(),
				"tipo":"kill",
                "channels": channels,
                "nick": "IRC Server",
                "msg":"Reason "+reason+" message: "+message+" nick "+nick
            });
        });

        // Emitted when a notice is sent.
        irc_clients[id].addListener('notice', function(nick, to, text, message) {
//          console.log(JSON.stringify(message));
            enviarParaCliente(id, {
                "timestamp": Date.now(),
				"tipo":"notice",
                "nick": "IRC Server",
                "msg":"NOTICE: "+message.args[1]
            });
        });


        // Emitted when a server PINGs the client.
        irc_clients[id].addListener('ping', function(server) {

            enviarParaCliente(id, {
                "timestamp": Date.now(),
                "nick": "IRC Server",
				"tipo":"ping",
                "servidor":server
            });
        });

		// Emitted when the server has finished returning a channel list. 
        irc_clients[id].addListener('channellist', function(channel_list) {
            enviarParaCliente(id, {				
                "timestamp": Date.now(),
                "nick": "IRC Server",
                "tipo": "channellist",
                "msg":channel_list
            });
        });

		// Emitted for each channel the server returns.
        irc_clients[id].addListener('channellist_item', function(channel_info) {
            enviarParaCliente(id, {
                "timestamp": Date.now(),
				"tipo": "channellist_item",	
                "nick": "IRC Server",
                "msg":channel_info
            });
        });

		// As per ‘part’ event but only emits for the subscribed channel. 
		irc_clients[id].addListener('part#teste', function(nick, reason, message) {
            enviarParaCliente(id, {
                "timestamp": Date.now(),
				"tipo":"part#teste",
                "nick": "IRC Server",
                "msg":message+" nick "+nick,
				"reason":reason
            });
        });
		
		// Emitted whenever the server finishes outputting a WHOIS response.
		irc_clients[id].addListener('whois', function(info) {
                  info='nick: '+info.nick+' - usuário: '+info.user+' - nome real: '+info.realname;
            enviarParaCliente(id, {
                "timestamp": Date.now(),
                "nick": "IRC Server",
				"tipo":"whois",							
                "msg":info			
            });
        });
		

		proxies[id] = irc_clients[id];
	});
	
	receberDoCliente("gravar_mensagem", function (msg) {        
		message = msg.msg.split(" ");
//		console.log("COMANDO: "+message[0]+" ARG: "+message[1]);
		if     (message[0] === 'JOIN'  ) irc_clients[msg.id].join(message[1]);    // (channel, callback)
		else if(message[0] === 'ACTION') irc_clients[msg.id].action(message[1],message[2]);  // (target, message)
		else if(message[0] === 'NOTICE') irc_clients[msg.id].notice(message[1],message[2]);  // (target, message)
		else if(message[0] === 'WHOIS' ) irc_clients[msg.id].whois(message[1]);   // (nick, callback)
		else if(message[0] === 'LIST'  ) irc_clients[msg.id].list();  // ([arg1, arg2, ...])				
		else if(message[0] === 'PART'  ) {	// Testado e aprovado, lembrar da # pro canal
			if(message[2] === '') irc_clients[msg.id].part(message[1]);
			else irc_clients[msg.id].part(message[1],message[2]); // (channel, message)					
		}
          else if(message[0] === 'NAMES') {
            if(message.length > 1) {
              irc_clients[msg.id].send(message[0], message[1]);
              }
            else {
              irc_clients[msg.id].send(message[0]);
            }
          }
		else{
		    irc_clients[msg.id].say(msg.canal, msg.msg);
		}
    });
}

// Recebe do Rabbit da lista do cliente
function receberDoCliente (canal, callback) {
	amqp_ch.assertQueue(canal, {durable: false});
//	console.log(" [irc] Waiting for messages in "+canal);
	amqp_ch.consume(canal, function(msg) {
//		console.log(" [irc] Received %s", msg.content.toString());
		callback(JSON.parse(msg.content.toString()));
	}, {noAck: true});
}

// Envia para o Rabbit redirecionar para o cliente
function enviarParaCliente (id, msg) {
//    console.log(JSON.stringify(msg));
	msg = new Buffer(JSON.stringify(msg));
    //msg = new Buffer(msg);
	amqp_ch.assertQueue("user_"+id, {durable: false});
	amqp_ch.sendToQueue("user_"+id, msg);
//	console.log(" [irc] Sent to ID "+id+ ": "+msg);
}




